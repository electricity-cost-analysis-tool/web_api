import bottle
import json
import tempfile
from bottle import route, response, request, hook
from beaker.middleware import SessionMiddleware
from tariffs import *
from tariffs.interval_usage_structures import *
from tariffs.interval_tariff_structures import *
from hbin import *
from ami_data_parser import *


@hook('before_request')
def setup_request():
    '''This service is a supportive service which is being called before any
    client request to get the session variable.
    '''
    request.session = request.environ['beaker.session']


@route('/upload/intervaldata', method='POST')
def upload_interval_data():
    '''This service is used to turn the smart meter data file uploaded by
    client into a ndarray. This ndarray will then be stored to a session
    variable to be re-used later on by usage and cost breakdown services.
    This service makes use of ami_data_parser module to read file and align
    the meter data file.
    It will save the uploaded file to a temporal file in the local
    system before reading it due to the limitation of ami_data_parser.
    A 200 code will be sent back to the client if file is uploaded successfully
    A 412 (precondition-failed) code will be sent back to the client if there
    is something wrong with the uploaded file (The uploaded file is not 1 year
    long or the format of the file is not supported by ami_data_parser module)
    A 500 code will be sent back to the client if something goes wrong with the
    uploading file process.
    '''
    tempfile_path = tempfile.NamedTemporaryFile().name
    meter_data_file = request.POST['upload_file']
    meter_data_file.save(tempfile_path)
    interval_data = None
    try:
        interval_data = read_origin_interval_data_file(tempfile_path)
        interval_data = align_year(interval_data)  # Start at January 1st.
        request.session['interval_data'] = interval_data.iloc[:, 1:].values  # Turn into ndarray (in this case a 2D array)
        response.status = 200
        return response
    except ValueError:
        response.status = 412
        return response
    except Exception as e:
        response.status = 500
        return response
#    return json.dumps(request.session.get('interval_data'))


@route('/breakdown/usage/quarterly')
def summarise_quarterly_usage():
    '''This service is used for usage breakdown module. The service will
    return a usage breakdown structure based on quarterly basis.
    @param the session variable which stores the transformed meter data file
    A 200 code will be sent back to the client if file is uploaded successfully
    A 500 code will be sent back to the client if something goes wrong with the
    uploading file process.
    '''
    try:
        quarterly_usage_summary = json.dumps(
            HBin.build(standard_usage_structure).binList(request.session.get('interval_data').flat).to_dict())
        response.status = 200
        response.content_type = 'application/json; charset=UTF-8'
        return quarterly_usage_summary
    except Exception:
        response.status = 500
        return response


@route('/breakdown/usage/monthly')
def summarise_monthly_usage():
    '''This service is used for usage breakdown module. The service will
    return a usage breakdown structure based ong monthly basis.
    @param the session variable which stores the transformed meter data file
    A 200 code will be sent back to the client if file is uploaded successfully
    A 500 code will be sent back to the client if something goes wrong with the
    uploading file process.
    '''
    try:
        monthly_usage_summary = json.dumps(HBin.build(flat_monthly_structure).binList(request.session.get('interval_data').flat).to_dict())
        response.status = 200
        response.content_type = 'application/json; charset=UTF-8'
        return monthly_usage_summary
    except Exception:
        response.status = 500
        return response


@route('/breakdown/cost/<tariff_name>')
def estimate_cost(tariff_name):
    '''This service is used to for estimated cost breakdown module.
    The service will calculate the estimated cost based on the
    <tariff_name> required by the user and the uploaded meter data.
    The response will include outright charge and temporal charge which
    consists of demand charge, fixed daily charge, and volume charge.
    @param the session variable which stores the transformed meter data file
    A 200 code will be sent back to the client if file is uploaded successfully
    A 500 code will be sent back to the client if something goes wrong with the
    uploading file process.
    '''
    tariff_volume_charge = None
    tariff = None
    try:
        interval_meter_data = request.session.get('interval_data')
        tariff = get_tariff_by_name(tariff_name)
        (outright_costs, temporal_costs) = tariff.apply(interval_meter_data.flat).flat()

        prepared_response = json.loads(cost_breakdown_response)
        prepared_response['charges']['outright_charge'] = outright_costs
        prepared_response['charges']['temporal_charge_labels'] = temporal_costs.columns
        prepared_response['charges']['temporal_charge']['monthly'] = HBin.build(
            flat_monthly_structure).binList(temporal_costs.values).to_dict()
        prepared_response['charges']['temporal_charge']['quarterly'] = HBin.build(
            standard_usage_structure).binList(temporal_costs.values).to_dict()
        prepared_response['tariff_description']['tariff_type'] = tariff._tariff['tariff_type']
        prepared_response['tariff_description']['description'] = tariff._tariff['description']
        prepared_response['tariff_description']['retailer'] = tariff._tariff['retailer']

        response.status = 200
        response.content_type = 'application/json; charset=UTF-8'
        return json.dumps(prepared_response, cls=ObjectStrJSONEncoder)
    except Exception as e:
        response.status = 500
        return response


@route('/hello/<name>')
def index(name):
    return template('<b>Hello {{name}}</b>!', name=name)


'''The response structure of the cost breakdown service '''
cost_breakdown_response = '''
{
    "charges": {
        "outright_charge": {},
        "temporal_charge_labels": [],
        "temporal_charge": {
            "monthly": {},
            "quarterly": {}
        }
    },
    "tariff_description": {
        "tariff_type": "tariff_type",
        "description": "description",
        "retailer": "retailer"
    }
}
'''


class ObjectStrJSONEncoder(json.JSONEncoder):

    '''
    The default encoder will not attempt to stringify object. Override to do so.
    '''

    def default(self, o):
        '''
        Called when JSONEncoder has already failed.
        If this fails call JSONEncoder again to let it generate TypeError.
        '''
        value = None
        try:
            iterable = iter(o)
            value = list(iterable)
        except TypeError:
            value = str(o)
        if value is None:
            json.JSONEncoder.default(self, o)
        return value


if __name__ == '__main__':
    session_options = {
        'session.type':'file',
        'session.data_dir':'./session/',
        'session.cookie_expires':3600,
        'session.auto':True
    }
    app = SessionMiddleware(bottle.default_app(), session_options)
    bottle.run(app=app, host='localhost', port=8080, reloader=True)
