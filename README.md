# Web API
This project contains a JSON+HTML ReST API for an electricity usage and cost analysis web application. All of the API endpoints are in the `app.py` script. This project uses Python `bottle` framework and `bottle-beaker` for server-side session management.

# End Points
There are 4 main endpoints exponsed from the API.

*NOTE:* The upload meter data file API must be called before any of the usage and cost breakdown services are called as other services are using the session variable created by the upload meter data file service as the input.

**Upload meter data file API:** <br/>

    Route: /upload/intervaldata
    Method: POST
    Description: This service is used to receive a meter data file uploaded by users and turn it into an usable ndarray. The transformed meter data file will be saved to a session variable called `interval_data` to be used in the other services.

**Quarterly usage breakdown APIs:** <br/>

    Route: /breakdown/usage/quarterly
    Method: GET
    Description: This service receives the input from the session variable `interval_data` and provide the usage breakdown based on `standard_usage_structure` as the response to client.
    Refer to `docs\back-end-output-sample\Usage-Breakdown-Quarterly-v1.1.json` file for further details of the response.

**Monthly usage breakdown APIs:** <br/>

    Route: /breakdown/usage/monthly
    Method: GET
    Description: This service receives the input from the session variable `interval_data` and provide the usage breakdown based on `flat_monthly_structure` as the response to client.
    Refer to `docs\back-end-output-sample\Usage-Breakdown-Monthly-v1.1.json` file for further details of the response.

**Estimated cost breakdown APIs:** <br/>

    Route: /breakdown/cost/\<tariff_name\>
    @tariff_name is a dynamic variable passed by front-end system.
    Method: GET
    Description: This service uses `tariff_name` variable to get the tariff structure on which the calculate estimated cost for the uploaded meter data file will be calculated. The response will includes:
        + outright_charge: environmetal charge, connection charge, upfront charge
        + temporal_charge: volume charge, demand charge, fixed daily charge.
    Refer to `docs\back-end-output-sample\Cost-Breakdown-Module-Output-v1.3.json` file for futher details.

# Requirements

  + Python >=3.5 and some dependendencies. See requirements.txt.
  + Apache >=2.4 installed.

# Installation

Optionally create a Python virtual env to work from:

    virtualenv -p python3.6 web_api_env
    source  web_api_env/bin/activate

Install dependendencies:

    apt-get install python3 apache2
    pip install -r requirements.txt

Configure mod_wsgi:

    mod_wsgi-express module-config
    pip install -r  requirements.txt

This command will show something like this:

    LoadModule wsgi_module "/home/ubuntu/arp_data/installation_guide_system/web_api/web_api_env/lib/python3.6/site-packages/mod_wsgi/server/mod_wsgi-py36.cpython-36m-x86_64-linux-gnu.so"
    WSGIPythonHome "/home/ubuntu/arp_data/installation_guide_system/web_api/web_api_env"
    Copy the line: LoadModule wsgi_module "/home/ubuntu/arp_data/installation_guide_system/web_api/web_api_env/lib/python3.6/site-packages/mod_wsgi/server/mod_wsgi-py36.cpython-36m-x86_64-linux-gnu.so" to /etc/apache2/mods-enabled/wsgi.load file
    Copy the line: WSGIPythonHome "/home/ubuntu/arp_data/installation_guide_system/web_api/web_api_env" to /etc/apache2/mods-enabled/wsgi.conf file

Create an Apache vhost configuration file. An example is provided in `tariff.conf`:

    <VirtualHost *:80>
    	ServerName comparetariff.com
    	DocumentRoot /var/www/ElecTariff
    	WSGIDaemonProcess compare_tariff user=ubuntu group=www-data processes=1 threads=5
    	WSGIScriptAlias /tariff /home/ubuntu/arp_data/installation_guide_system/web_api/app.wsgi
    	<Directory /home/ubuntu/arp_data/installation_guide_system/web_api>
    		WSGIProcessGroup compare_tariff
    		WSGIApplicationGroup %{GLOBAL}
    		Require all granted
    	</Directory>
    	ErrorLog ${APACHE_LOG_DIR}/error.log
    	CustomLog ${APACHE_LOG_DIR}/access.log combined
    </VirtualHost>

Please note to change the directory and folder name in the `tariff.conf` file properly to the directory of your own installation:

    # Reload apache:
    sudo service apache2 reload

# Testing
`python3 tests-inf/test_web_api.py` will start a local dev server automatically and run some tests. You can also run the bottle application stand alone by executing `app.py` which will start a development server on localhost. From there you can test manually or with script in `tests-inf/`.
