import sys
import inspect
import json
from os.path import dirname, realpath
from beaker.middleware import SessionMiddleware
from webtest import TestApp, forms
from bottle import request
from http.cookiejar import CookieJar
import collections
sys.path.append(dirname(realpath(__file__)))
from expected_APIs_output import *


test_data_path = dirname(realpath(__file__)) + '/test-data/'


def test_usage_and_cost_breakdown():
    app = TestApp('http://127.0.0.1:8080')
    resp = app.post('/upload/intervaldata', collections.OrderedDict(
        [(('upload_file'), forms.Upload(test_data_path + '/sample-ami-meter-data-origin-20160401_20170401.csv'))]))

def test_valid_file_uploading():
    app = TestApp('http://127.0.0.1:8080')
    meter_data_file_types = ['type_a.csv', 'type_b.csv', 'type_c.csv', 'type_d.csv', 'type_e.csv', 'type_f.csv', 'type_g.csv']
    for meter_data_file_type in meter_data_file_types:
        resp = app.post(
            '/upload/intervaldata',
            collections.OrderedDict([(
                ('upload_file'),
                forms.Upload(test_data_path + meter_data_file_type)
            )])
        )
        print('Testing the uploading file for file format', meter_data_file_type, '......')
        assert resp.status == '200 OK'
        print('Uploading file for file format', meter_data_file_type, 'passed.')


def test_invalid_file_uploading():
    app = TestApp('http://127.0.0.1:8080')
    try:
        print('Testing the uploading invalid file......')
        resp = app.post('/upload/intervaldata', collections.OrderedDict([(('upload_file'), forms.Upload('type_invalid.csv'))]))
        print('ERROR: Uploading invalid file test failed!')
    except Exception as e:
        print(e)
        print('Uploading invalid file passed.')


test_usage_and_cost_breakdown()
test_valid_file_uploading()
test_invalid_file_uploading()
