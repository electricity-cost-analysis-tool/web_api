#!/bin/bash -e
cd $(dirname $0)
BASE_URI=${1:-"127.0.0.1:8080"}
TEST_TARIFF_NAME=
TEST_AMI_DATA_FILE="./test-data/sample-ami-meter-data-origin-20160401_20170401.csv"
echo "Using HOST=${BASE_URI}; FILE=${TEST_AMI_DATA_FILE}"
echo "Uploading a meter data file [cookies to/from cookies.txt]"
curl "${BASE_URI}/upload/intervaldata" -F "upload_file=@${TEST_AMI_DATA_FILE}" -c cookies.txt -b cookies.txt -v
echo "Ask for quarterly breakdown"
curl "${BASE_URI}/breakdown/usage/quarterly" -c cookies.txt -b cookies.txt -v
echo "Ask for monthly breakdown"
curl "${BASE_URI}/breakdown/usage/monthly" -c cookies.txt -b cookies.txt -v
echo "Ask for cost breakdown [${TEST_TARIFF_NAME}]"
curl "${BASE_URI}/breakdown/cost/sample_full_tariff_1" -c cookies.txt -b cookies.txt -v
echo -e "\nDone"
