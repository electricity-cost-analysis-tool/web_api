import os
import sys
import bottle
os.chdir(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
import app
from beaker.middleware import SessionMiddleware
from app import *

session_options = {
  'session.type':'file',
  'session.data_dir':'./session/',
  'session.cookie_expires':3600,
  'session.auto':True
}

application = SessionMiddleware(bottle.default_app(), session_options)